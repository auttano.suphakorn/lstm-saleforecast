import LSTM as Model
import matplotlib.pyplot as plt
import pandas as pd
Model.DataColumn = 'SALE'
FutureSize = Model.FutureSize = 3

# Instantiate and train the predictor
csv_file_path = 'Alcohol_Sales.csv'
predictor = Model.TimeSeriesPredictor(csv_file_path)
predictor.train()

# Make predictions and visualize results
future_predictions = predictor.predict(12)

# แสดงกราฟ
plt.plot(predictor.df.index[:-predictor.test_size], predictor.train_set, label='Training Data')
plt.plot(predictor.df.index[-predictor.test_size:], predictor.test_set, label='True Future Data')
# plt.plot(predictor.df.index[-predictor.test_size:], future_predictions,label='Predicted Future Data')
plt.legend()
plt.show()