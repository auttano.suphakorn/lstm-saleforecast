import torch
import torch.nn as nn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
import time

DataColumn = 'SALE'
FutureSize = 18

register_matplotlib_converters()

class SalesPredictor(nn.Module):
    def __init__(self, input_size=1, hidden_size=100, output_size=1):
        super().__init__()
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(input_size, hidden_size)
        self.linear = nn.Linear(hidden_size, output_size)
        self.hidden = (torch.zeros(1, 1, self.hidden_size),
                       torch.zeros(1, 1, self.hidden_size))

    def forward(self, seq):
        out, self.hidden = self.lstm(seq.view(len(seq), 1, -1), self.hidden)
        pred = self.linear(out.view(len(seq), -1))
        return pred[-1]

class TimeSeriesPredictor:
    def __init__(self, csv_file, input_size=1, hidden_size=100, output_size=1, window_size=12, lr=0.001, epochs= 32):
        self.df = pd.read_csv(csv_file, index_col=0, parse_dates=True)
        self.df.dropna(inplace=True)

        self.y = self.df[DataColumn].values.astype(float)
        self.test_size = 12
        self.train_set = self.y[:-self.test_size]
        self.test_set = self.y[-self.test_size:]

        self.scaler = MinMaxScaler(feature_range=(-1, 1))
        self.train_norm = self.scaler.fit_transform(self.train_set.reshape(-1, 1))

        self.train_norm = torch.FloatTensor(self.train_norm).view(-1)

        self.window_size = window_size

        self.predictorModel = SalesPredictor(input_size, hidden_size, output_size)
        self.criterion = nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.predictorModel.parameters(), lr=lr)

        self.epochs = epochs

    def input_data(self, seq):
        out = []
        L = len(seq)
        for i in range(L - self.window_size):
            window = seq[i:i + self.window_size]
            label = seq[i + self.window_size:i + self.window_size + 1]
            out.append((window, label))
        return out

    def train(self):
        train_data = self.input_data(self.train_norm)
        start_time = time.time()

        for epoch in range(self.epochs):
            for seq, y_train in train_data:
                self.optimizer.zero_grad()
                self.predictorModel.hidden = (
                    torch.zeros(1, 1, self.predictorModel.hidden_size),
                    torch.zeros(1, 1, self.predictorModel.hidden_size)
                )
                y_pred = self.predictorModel(seq)
                loss = self.criterion(y_pred, y_train)
                loss.backward()
                self.optimizer.step()
            print(f'Epoch: {epoch + 1:2} Loss: {loss.item():10.8f}')

        print(f'\nDuration: {time.time() - start_time:.0f} seconds')

    def predict(self, FutureSize):
        preds = self.train_norm[-self.window_size:].tolist()
        self.predictorModel.eval()

        for i in range(FutureSize):
            seq = torch.FloatTensor(preds[-self.window_size:])
            with torch.no_grad():
                self.predictorModel.hidden = (
                    torch.zeros(1, 1, self.predictorModel.hidden_size),
                    torch.zeros(1, 1, self.predictorModel.hidden_size)
                )
                preds.append(self.predictorModel(seq).item())

        true_prediction = self.scaler.inverse_transform(np.array(preds[self.window_size:]).reshape(1, -1))
        true_prediction = true_prediction.squeeze()

        return true_prediction
    
# csv_file_path = 'Alcohol_Sales.csv'
# predictor = TimeSeriesPredictor(csv_file_path)
# predictor.train()

# # ทำนายข้อมูลในอนาคต
# future_predictions = predictor.predict(FutureSize)

# # แสดงกราฟ
# plt.plot(predictor.df.index[:-predictor.test_size], predictor.train_set, label='Training Data')
# plt.plot(predictor.df.index[-predictor.test_size:], predictor.test_set, label='True Future Data')
# plt.plot(pd.date_range(start=predictor.df.index[-1], periods=19, freq='M')[1:], future_predictions,
#          label='Predicted Future Data')
# plt.legend()
# plt.show()